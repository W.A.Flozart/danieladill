---
title: "Kontakt"
date: 2020-08-14T01:30:56+02:00
---

<div class="card mb-5">
  <div class="card-body">
    <h2 class="card-title">Kontakt</h2>
  </div>
  <div class="d-flex justify-content-evenly">
    <img src="../images/kontakt/pic01.jpg" class="card-img-top" alt="...">
  </div>
  <div class="card-body">
    <p class="card-text">M: <a href="mailto:info@danieladill.ch">info@danieladill.ch</a> <br> T: +41 79 302 54 25</p>
  </div>
</div>