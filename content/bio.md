---
title: "Bio"
date: 2020-08-14T01:30:56+02:00
---


<div class="px-2 pt-1 cbg">
<p class="biotext">
DANIELA DILL ist Basler Spoken-Word-Künstlerin, Autorin und Kulturvermittlerin; studierte Französische und Deutsche Literaturwissenschaften an der Universität Basel, nahm ab 2007 umtriebig an Poetry Slams teil, organisierte einige Veranstaltungen rund um Slam und Spoken Word in der Region Basel und im Aargau; war wissenschaftliche Mitarbeiterin im Dichter:innen- und Stadtmuseum Liestal (2011-2017), Mitbegründerin und Teammitglied vom WORTSTELLWERK – Junges Schreibhaus Basel 2019-2024), ist Mitglied der Spoken-Word-Band <a href="https://dillundkraut.ch">DILL & KRAUT</a> und führt mit Dominik Muheim die Lesebühne-Reihe «Gschichte vom Miststock» im Cheesmeyer Sissach.
<p class="biotext  mb-1">
Daniela Dill gibt Workshops für kreatives Schreiben und Bühnenperformance an Kinder, Jugendliche und Erwachsene.
</p>
</p>
</div>

<img src="../images/bio/BIO.png" style="border: solid black;">



