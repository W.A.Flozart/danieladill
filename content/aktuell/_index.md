---
title: "Aktuell"
type: "posts"
date: 2018-02-13T12:21:57-05:00

---
<div class="card mb-5" >
    <div class="card-body">
      <h2 class="card-title"><a href="https://vorlesungsverzeichnis.unibas.ch/de/vorlesungsverzeichnis?id=291341">«Hören, lesen, schreiben, fühlen: Wer bestimmt Literatur?»</a>
      </h2>
    <p class="card-text">
      Seminar im Masterstudiengang Literaturwissenschaften mit Eva Seck im Frühjahrssemester 2025 an der Universität Basel
      </p>
      <figure class="figure mb-0">
        <img src="../images/aktuell/KanonLiteratur_LIWIVerlag.jpg" alt="Seminar im Masterstudiengang Literaturwissenschaften">
        <figcaption class="figure-caption">Bildquelle: LIWI Verlag (<a href="https://liwi-verlag.de">liwi-verlag.de</a>)
      </figure class="figure">
    </div>
  </div>


<div class="card mb-5" >
    <div class="card-body">
      <h2 class="card-title">Eine Collagen-Werkschau im Rahmen des  Scissor*hood  Album Release
      von Les Reines Prochaines
      </h2>
      <p class="card-text">
      am Samstag 7. Dezember 2024 um 18.00 im Wettsteinhäuschen, Claragraben 38, mit Beiträgen von Michèle Fuchs, Sibylle Hauert, Barbara Naegelin, David Kermann, Iris Ganz, Martina Gmür, Ensemble Fimene, Andrea Saemann, Judith Huber, Gisela Hochueli, Gaby Streiff, Daniela Dill, Chris Hunter.
      </p>
      <img src="../images/aktuell/Collage_DebatteMitWurstUndBrot.jpeg" alt="Collagen-Werkschau">
    </div>
  </div>

<div class="card mb-5" >
    <div class="card-body">
      <h2 class="card-title">Daniela Dill & Les Reines Prochaines am woerdz festival Luzern /// Werkauftrag (Uraufführung)</h2>
      <p class="card-text">
      Für den Werkauftrag hat sich die Basler Spoken-Word-Künstlerin <a href="https://woerdz.ch/artist/daniela-dill/">Daniela Dill</a> mit den legendären <a href="https://woerdz.ch/artist/les-reines-prochaines/">Les Reines Prochaines</a> zusammengetan; es wird rezitiert, gesungen und getanzt.
      </p>
        <img src="../images/aktuell/woerdz-2024-2-scaled.jpg" alt="woerdz festival">
    </div>
  </div>

<div class="card mb-5" >
    <div class="card-body">
      <h2 class="card-title">Spoken Word im Kreuz an der Kulturnacht Solothurn
      27. April 2024
      </h2>
      <a href="https://kulturnachtsolothurn.ch/events/spoken-word-mit-daniela-dill">
        <img src="../images/aktuell/KulturnachtSolothurn.jpg" alt="Kulturnacht Solothurn">
        </a>
    </div>
  </div>

<div class="card mb-5" >
    <div class="card-body">
      <h2 class="card-title">46. Solothurner Literaturtage</h2>
      <p class="card-text">
      Daniela Dill thurnt Solo und mit Band in Solothurn an den Literaturtagen. 
      </p>
      <a href="https://www.literatur.ch/de/programm/?slot=41256">
        <img src="../images/aktuell/SolothurnerLiteraturtage.jpg" alt="46. Solothurner Literaturtag">
        </a>
    </div>
  </div>

<div class="card mb-5" >
    <div class="card-body">
      <h2 class="card-title">DIE OPERNSÄNGERIN</h2>
      <p class="card-text">DIE OPERNSÄNGERIN kriegt endlich ihre Bühne und feiert am 8. März 2024 Premiere in Küssnacht am Rigi. <br>Weitere Vorstellungstermine sowie der Link zur Ticketreservation sind auf <a href="https://www.kuessnachter-theaterleute.ch">www.kuessnachter-theaterleute.ch</a></p>
        <img src="../images/aktuell/Operesaengerin.jpg" alt="Die Opernsängerin">
     <p class="card-text">
      Spiel:
      Laura Baumberger, Chantal Gattone, Patrick Hediger, Chantal Kaufmann, Philipp Michel, Hilde Nigg, Milena Pfister, Simone Ulrich, Julia Vonwyl <br>
      Produktion:
      Christof Bühler (Regie/Szenografie),
      Sandro Griesser (Regie/Dramaturgie),
      Daniela Dill (Text),
      Simone Felber (Musik),
      Clara Sollberger (Kostüme),
      Flavia Bienz (Kostüme),
      Markus Güdel (Lichtdesign),
      Nora Ly (Produktionsleitung)
      </p>
    </div>
  </div>


<div class="card mb-5" >
    <div class="card-body">
      <h2 class="card-title">"Gegen den Glanz" nun online erhältlich</h2>
      <p class="card-text">GEGEN DEN GLANZ ist unsere erste Studioaufnahme. 
  Ab heute ist sie auf <a href="https://lnk.site/dill-und-kraut-gegen-den-glanz">verschiedenen Plattformen</a> anzuhören und kann runtergeladen werden.
  <br>
  <br>Auf ein fetziges Hörerlebnis!
      </p>
    </div>
    <a href="https://lnk.site/dill-und-kraut-gegen-den-glanz">
    <div class="d-flex justify-content-evenly">
        <img src="../images/aktuell/CoverDK.png" alt="Cover von Gegen den Glanz">
    </div>
    </a>
  </div>


<div class="card mb-5" >
  <div class="card-body">
    <h2 class="card-title">WORTKNALL - Spoken Word in der Schweiz</h2>
    <h5 class="card-subtitle mb-2 text-muted">Textsammlung, edition spoken script 40</h5>
  </div>
  <div class="d-flex justify-content-evenly">
    <a class="image-link" href="https://menschenversand.ch/produkt/spoken-word-wortknall/" target="_blank">
      <img src="../images/aktuell/Wortknall.gif" alt="...">
    </a>
  </div>
  <div class="card-body">
    <p class="card-text">Mit: Amina Abdulkadir, Andri Beyeler, Laurence Boissier, Katja Brunner, Renata Burckhardt, Martina Clavadetscher, Cruise Ship Misery (Sarah Elena Müller, Milena Krstic), Daniela Dill, Michael Fehr, Heike Fiedler, Martin Frank, Anna Frey, Meloe Gennai, Nora Gomringer, Ariane von Graffenried, Stefanie Grob, Pablo Haller, Jürg Halter, Rolf Hermann, Franz Hohler, Antoine Jaccoud, Jurczok 1001, Matto Kämpf, Judith Keller, Guy Krneta, Sandra Künzi, Pedro Lenz, Patric Marino (Die Astronauten), Gerhard Meister, Marko Miladinovic, Melinda Nadj Abonji, Narcisse, Jens Nielsen, Dominic Oppliger, Achim Parterre, Dragica Rajčić Holzner, Noëlle Revaz, Béla Rothenbühler, Anja Nora Schulthess, Christoph Simon, Marina Skalova, Michael Stauffer, Beat Sterchi, Christian Uetz.</p>
  </div>
</div>

<div class="card mb-5">
  <div class="card-body">
    <h2 class="card-title">Durzueständ an den Solothurner Literaturtagen</h2>
    <h5 class="card-subtitle mb-2 text-muted">vom 14.-16. Mai 2021</h5>
  </div>
  <div class="d-flex justify-content-evenly">
      <img src="../images/aktuell/SLT-Flyer-AutorInnen-20200218-SCREEN.jpg" alt="...">
  </div>
</div>

<div class="card mb-5">
  <div class="card-body">
    <h2 class="card-title">Durzueständ in der Schnabelweid</h2>
     <h5 class="card-subtitle mb-2 text-muted">vom 26. November 2020</h5>
  </div>
  <div class="d-flex justify-content-evenly">
    <a class="image-link" href="https://www.srf.ch/play/radio/schnabelweid/audio/daniela-dill-dur-zue-staend?id=9445c44d-b406-4d28-967a-b6d6ac4033e6&fbclid=IwAR0tVBKW0tUjzbTE4i0Ke3zGCayCDtXjKnKeYKJ3SyMJwdHZ5v-H7Glw61Y" target="_blank">
      <img src="../images/aktuell/1353325992.jpg" class="card-img-top" alt="...">
    </a>
  </div>
  <div class="card-body">
    <p class="card-text">Danke Simon Leuthold für die schöne und abwechslungsreiche Sendung.</p>
  </div>
</div>

<div class="card mb-5">
  <div class="card-body">
    <h2 class="card-title">Das Buch sei ab heute im Buchhandel.</h2>
     <h5 class="card-subtitle mb-2 text-muted">vom 26. November 2020</h5>
    <p class="card-text">
        Danke Simon Leuthold für die schöne und abwechslungsreiche Sendung.
    </p>
  </div>
  <div class="d-flex justify-content-evenly">
    <img src="../images/aktuell/IMG_5940.jpg" class="card-img-top" alt="...">
  </div>
  <div class="card-body">
    <p class="card-text">Merci, Christoph Simon, für den Hinweis.</p>
  </div>
</div>