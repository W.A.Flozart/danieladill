---
title: "Live"
date: 2020-08-14T01:30:56+02:00
---
<img src="../images/2016seebad.jpg" class="dklogo d-none d-md-block" style="margin-top:0;">
<div class="d-block px-2 cbg">

<hr class="my-0 d-none d-xl-block" style="width: 100%; color: #ff6680">
<div class="table-responsive">

 |  |  |  
---- | ---- | ----- | ----
<span class="table-year">2025</span> |  |  | 
... | |Auftrittspause bis Mai 2025



</div>
<h2 class="mt-5 mb-4 ps-2">Vergangene Auftritte</h2>


<div class="accordion accordion-flush" id="accordionFlushExample">

<div class="accordion-item accordion-year my-3">
    <h2 class="accordion-header" id="flush-heading23">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse24" aria-expanded="false" aria-controls="flush-collapse24">
        2024
      </button>
    </h2>
    <div id="flush-collapse24" class="accordion-collapse collapse" aria-labelledby="flush-heading24" data-bs-parent="#accordionFlushExample">
      <div class="accordion-body">
        <div class="table-responsive">

|  |  |  
---- | ---- | ----- 
<span class="dk">04.10.</span> |<span class="dk">20:15</span>|<span class="dk">GEGEN DEN GLANZ </span>|<span class="dk"> <a href="https://odeon-brugg.ch/theater/">Odeon</a>, Brugg AG</span>
<span class="dk">20.09.</span> |<span class="dk">20:00</span>|<span class="dk">GEGEN DEN GLANZ </span>|<span class="dk"> <a href="https://www.kulturforum-amriswil.ch/">Kulturforum</a>, Amriswil TG</span>
<span class="dk">07.09.</span> |<span class="dk">20:15</span>|<span class="dk">GEGEN DEN GLANZ </span>|<span class="dk"><a href="https://www.assel.ch/">Asselkeller</a>, Schönengrund AR</span>
<span class="dk">31.07.</span> |<span class="dk">tba</span>|<span class="dk">Sommerfest </span>|<span class="dk"><a href="https://www.tannhuesern.ch/">Tannhüsern</a>, Hellbühl LU</span>
06.06.|19:00|<a href="https://cheesmeyer.ch/cheesmeyer_event/muheim-dill-4/">Blueme vom Miststock mit Dominik Muheim + Rebekka Salm</a>|Cheesmeyer, Sissach
11.05.|14:30|46. Solothurner Literaturtage<a href="https://www.literatur.ch/de/programm/?slot=41256">„Omegäng“ – eine literarische Vertiefung</a>|Landhausaal, Solothurn
<span class="dk">11.05.</span>|<span class="dk">19:00</span>|<span class="dk"><a href="https://www.literatur.ch/de/gaeste/">46. Solothurner Literaturtage</span>|<span class="dk"> <a href="https://www.literatur.ch/de/gaeste/"><a href="https://www.kino-uferbau.ch/">Kino im Uferbau</a>, Solothurn</span>
27.04. | 18:00 | <a href="https://kulturnachtsolothurn.ch/events/spoken-word-mit-daniela-dill">19. Kulturnacht Solothurn</a> | Spoken Word Kreuz, Solothurn
10.04. | 16:30 |46. Solothurner Literaturtage<a href="https://www.literatur.ch/de/programm/?slot=44900"> Kurzlesung</a> | en plein air, Solothurn
<span class="dk">17.04.</span> |<span class="dk">20:00</span>|<span class="dk">DILL&KRAUT "GEGEN DEN GLANZ"</span> | <span class="dk"><a href="https://www.herzbaracke.ch/ecm/ecm.cgi?c=index.html">Herzbaracke,</a> Rapperswil</span>
14.03.|20:00|Queer Slam|<a href="https://www.millers.ch/">Millers,</a> Zürich        
<span class="dk">28.02.</span> |<span class="dk">20:00</span>|<span class="dk">DILL&KRAUT "GEGEN DEN GLANZ"</span> | <span class="dk"><a href="https://www.herzbaracke.ch/ecm/ecm.cgi?c=index.html">Herzbaracke,</a> Bellevue Zürich</span>
<span class="dk">02.02.</span> |<span class="dk">20:00</span>|<span class="dk">DILL&KRAUT "GEGEN DEN GLANZ"</span> | <span class="dk"><a href="https://www.pfarrhauskeller.ch/programm-1">Theater Pfarrhauskeller, Waldenburg</span>
</div>
      </div>
    </div>
       </div>

<div class="accordion accordion-flush" id="accordionFlushExample">

<div class="accordion-item accordion-year my-3">
    <h2 class="accordion-header" id="flush-heading23">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse23" aria-expanded="false" aria-controls="flush-collapse23">
        2023
      </button>
    </h2>
    <div id="flush-collapse23" class="accordion-collapse collapse" aria-labelledby="flush-heading23" data-bs-parent="#accordionFlushExample">
      <div class="accordion-body">

<div class="table-responsive">

|  |  |  
---- | ---- | ----- 
<span class="dk">14.12.</span> | <span class="dk">20:00</span> |<span class="dk">✨PLATTENWURF✨ GEGEN DEN GLANZ</span> |<span class="dk"> <a href="https://www.barakuba.ch/">Barakuba,</a> Basel --- AUSVERKAUFT</span>
<span class="dk">08.12.</span> |<span class="dk">15:00</span>|<span class="dk"><a href="https://lnk.site/dill-und-kraut-gegen-den-glanz">Digital-Release von "GEGEN DEN GLANZ"</a></span> |<span class="dk">Online</span>
<span class="dk">22.11.</span> |<span class="dk">20:00</span>|<span class="dk">GEGEN DEN GLANZ</span> | <span class="dk"><a href="https://www.herzbaracke.ch/ecm/ecm.cgi?c=index.html">Herzbaracke</a>, Bellevue Zürich</span>
02.11. |19:00 |MUHEIM & DILL und Gast, Spoken Word |<a href="https://cheesmeyer.ch/">Cheesmeyer,</a> Sissach
24.10. |20:00 |Daniela Dill & Martina Clavadetscher |<a href="https://logeluzern.ch/">Loge,</a> Luzern
21.10. |20:00 |Kulturnacht Burgdorf |<a href="https://www.kulturnacht-burgdorf.ch/veranstaltungen/liste/?tribe-bar-search=Dill">Buchhandlung am Kronenplatz</a>
19.10. |14:30 | Frankfurter Buchmesse |Halle 3.1 F122, Frankfurt
<span class="dk">06.10.</span> |<span class="dk">20:00</span> | <span class="dk">DILL&KRAUT "Ds Beschte vom Beschte"</span> |<span class="dk"><a href="https://arosalenzerheide.swiss/de/Arosa/Top-Events/Kultur-und-Musik/Mundartfestival">Mundartfestival 
05.10. |20:00 | "DINI MUNDART" – Baselbieter Abend mit Rebekka Salm, Markus Gasser, Dominik Muheim, Stefan Uehlinger & Florian Schneider / Moderation: Seraina Degen  |LIVE <a href="https://www.srf.ch/audio/radio-srf-1-live-hoeren">Radio SRF 1</a>
20.09. | tba | „Dem Volk aufs Maul geschaut“ | theater palazzo Liestal
<span class="dk">13.05.</span> | <span class="dk">20.00</span> | <span class="dk">DILL&KRAUT – Gegen den Glanz</span> | <span class="dk"><a href="https://haerdoepfuchaeuer.ch">Härdöpfuchäuer</a></span>
09.05. |20.00  | Lesung u.a. aus „Durzueständ“ | Bücher Dillier Sarnen
<span class="dk">22.04.</span> | <span class="dk">13.00</span>| <span class="dk"><a href="https://www.kuenstlerboerse.ch/"></a>DILL&KRAUT – Schweizer Kunstbörse</span> | <span class="dk">Schadausaal Thun</span>
<span class="dk">01.04.</span> | <span class="dk">18.00</span> | <span class="dk">DILL&KRAUT – Gegen den Glanz</span> | <span class="dk"><a href="https://www.raumformer.ch/news#comp-ledfcy9w">Raumformer Zuchwil</a></span>
<span class="dk">11.03.</span> | <span class="dk">DILL & KRAUT</span> |<a href="https://kleinbuehne-chupferturm.ch">Kleine Bühne Chupferturm</a>, <span class="dk">Schwyz</span>
16.03. | Gschichte vom Miststock | Cheesmeyer Sissach
<span class="dk">11.03.</span> | <span class="dk">DILL&KRAUT – Gegen den Glanz</span> | <span class="dk">Chupferturm Schwyz</span>
04.03. | Lesung am Literaare | Mundwerk Thun
22.02 | Doing-Family-Slam | Vögele Kulturzentrum Pfäffikon
17.02. | Lesung bei Kollektiv Kitzeln | Stauffacher Bern
      </div>
      </div>
    </div>
  </div>
  </div>
  </div>
</div>

  <div class="accordion-item accordion-year my-3">
    <h2 class="accordion-header" id="flush-heading2">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse22" aria-expanded="false" aria-controls="flush-collapse22">
        2022
      </button>
    </h2>
    <div id="flush-collapse22" class="accordion-collapse collapse" aria-labelledby="flush-heading22" data-bs-parent="#accordionFlushExample">
      <div class="accordion-body">

<div class="table-responsive">

|  |  |  
---- | ---- | ----- 
03.12. | 20:00 | Lesung & Mariann Bühler	| WABE, Basel 
<span class="dk">30.12.</span> | <span class="dk">tba</span> | <span class="dk">DILL & KRAUT</span> | <span class="dk"><a href="https://www.chaeslager-kulturhaus.ch/">Chäslager</a>, Stans</span>
<span class="dk">28.10.</span> |  <span class="dk">DILL & KRAUT</span>	| <span class="dk">Nebia poche, Biel</span>
<span class="dk">04.11.</span> |  <span class="dk">DILL & KRAUT</span>	| <span class="dk">Wydekantine, Dornach</span>
15.10. |  Kulturgaragen-Slam	| Kulturgarage, Interlaken
16.10. |  Turm-Gespräch mit Dominik Muheim | Kulturbeiz, Basel
09.10. |  Mundart-Festival	| Kursaal, Arosa
07.10. |  Mundart-Festival	| Kursaal, Arosa
<span class="dk">28.09.</span> |  <span class="dk">DILL & KRAUT</span>	| <span class="dk">Barakuba, Basel</span>
16.09. | 10 Jahre SLAM Basel | Sommercasino, Basel	
11.09. | 50-Jahre HAZ: Queer-Slam-Show | Theater im Zollhaus, Zürich
08.08. | Spoken-Word-Lesung | Frauenkloster, Ilanz 
<span class="dk">05.07.</span> | <span class="dk">DILL & KRAUT - Gegen den Glanz</span> | <span class="dk">WKS, Bern </span>
22.05. | Spoken-Word-Lesung am Mundartabend | Schlachthof, Lahr (D)
17.05. | Poetry Slam u.a. mit Marguerite Meyer | Dachstock, Bern 
<span class="dk">24.05.</span> | <span class="dk">DILL & KRAUT - Gegen den Glanz</span> | <span class="dk">Garage, Wetzikon</span>
05.05. | Mundartnacht "Gägäwärt" | Kofmehl, Solothurn | 
<span class="dk">30.04.</span> | <span class="dk">DILL & KRAUT - Gegen den Glanz</span> | <span class="dk">The Millers Studio, Zürich (abgesagt)</span> | 
28.04. | Spoken-Word-Lesung aus "Durzueständ" | Aprillen Festival, Bern | 
<span class="dk">23.04.</span> | <span class="dk">DILL & KRAUT 'Gegen den Glanz'</span> | <span class="dk">Solothurner Kulturnacht, Kantonsratsaal</span> | 
12.04. | Spoken Word-Lesung aus "Durzueständ" | Loge, Luzern | 
<span class="dk">18.03.</span> | <span class="dk">DILL & KRAUT - Gegen den Glanz im Nachtcafé</span> | <span class="dk">theater basel, Basel</span> | 
<span class="dk">26.02.</span> | <span class="dk">DILL & KRAUT: Gegen den Glanz</span> | <span class="dk">Theater am Bahnhof, Reinach</span> | 
09.02. | Spoken-Word-Abend mit Dominik Muheim & Daniela Dill | Stadtkeller, Dietikon
      </div>
      </div>
    </div>
  </div>
  </div>
  </div>
</div>

  <div class="accordion-item accordion-year my-3">
    <h2 class="accordion-header" id="flush-headingOne">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse21" aria-expanded="false" aria-controls="flush-collapse21">
        2021
      </button>
    </h2>
    <div id="flush-collapse21" class="accordion-collapse collapse" aria-labelledby="flush-heading21" data-bs-parent="#accordionFlushExample">
      <div class="accordion-body">
<div class="table-responsive">

|  |  |  
---- | ---- | ----- | 
<span class="dk">27.11.</span> |<span class="dk">DILL & KRAUT - Gegen den Glanz</span> | <span class="dk">Kulturnacht Lichtblicke, Liestal </span>
13.11. |"Durzueständ" - Spoken-Word-Lesung | Gemeindesaal, Lausen 
12.11. | Privatveranstaltung |
<span class="dk">06.11.</span> |<span class="dk">DILL & KRAUT - Gegen den Glanz</span> | <span class="dk">Buchbasel, KLARA </span>
04.11. |"Durzueständ" - Spoken-Word-Lesung am Buchfestival Olten | Stadtbibliothek, Olten 
25.09. |Poetry Slam Gala | Pfauen, Zürich 
23.09. |Spoken-Word-Lesung aus "Durzueständ" | Kulturhalle Konstanz, Rothenburg 
<span class="dk">14.09.</span> |<span class="dk">DILL & KRAUT im Rahmen des Podiums "Kunst kommt von Können"</span> | <span class="dk">Barakuba, Basel</span> 
01.09. |[Auf den Tisch gelegt] - Lesung im KLUB DIGESTIF | Kunstraum DOCK, Basel 
28.08. |Spoken-Word-Lesung | Klus Aesch 
19.08. |Spoken-Word-Lesung mit "Lauschig" | Musikfestwoche Winterthur |
08.07. |Sommer Soirée Stadtluft | Basler Stadthaus 
24.06. |Midsummer Night mit Ulrike Ulrich, Guy Krneta & Werner Rohner | Literaturhaus Zentralschweiz, Stans 
29.05. |Female Trouble | Miller's Studio, Zürich 
27.05. |Female Trouble | Miller's Studio, Zürich 
17.05. |Olympia Slam von und mit Christoph Simon | La Cappella, Bern
14.05. |Resonanzraum | Solothurner Literaturtage 
14.05. |Jukebox-Lesung mit Werner Rohner, Isabelle Sbrissa, Flavio Stroppini und Odile Cornuz | Solothurner Literaturtage 
14.05. |Spoken-Word-Lesung aus "Durzueständ" | Solothurner Literaturtage 
24.03. |Ohrfeigen aus dem Kleintheater Luzern | Spasspartout, SRF 1 
11.02. |Lesung aus "Durzueständ" | Literaturhaus Basel
      </div>
      </div>
    </div>
  </div>
  </div>
  </div></div>

<div class="accordion-item accordion-year my-3">
    <h2 class="accordion-header" id="flush-headingOne">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse20" aria-expanded="false" aria-controls="flush-collapse20">
        2020
      </button>
    </h2>
    <div id="flush-collapse20" class="accordion-collapse collapse" aria-labelledby="flush-heading20" data-bs-parent="#accordionFlushExample">
      <div class="accordion-body">
<div class="table-responsive">

|  |  |  
---- | ---- | ----- | 
"Literarischer Silvester" | Theater Stans (abgesagt) 
Olympia Slam mit Christoph Simon | La Capella Bern (abgesagt) 
<span class="dk">DILL & KRAUT Lichtblicke Liestal</span>  | <span class="dk">Liestal(abgesagt) </span> 
<span class="dk">DILL & KRAUT</span>  | <span class="dk">sogar theater, Zürich </span> 
Spoken Word 'Durzueständ' an der Sofa-Lesung | Wettingen 
<span class="dk">DILL & KRAUT an der BuchBasel</span>  | <span class="dk">Volkshaus, Basel (abgesagt) </span> 
Spoken Word 'Durzueständ' am Buchfestival Olten, u.a. Schützi | Olten (abgesagt) 
<span class="dk">DILL & KRAUT am woerdz-Festival Luzern</span>  | <span class="dk">Südpol </span> 
<span class="dk">DILL & KRAUT an der Kulturnacht Burgdorf</span>  | <span class="dk">Burgdorf, Stadtbibliothek </span> 
Buch-Vernissage 'Durzueständ' | Dichter- und Stadtmuseum, Liestal 
Spok'N'Music präsentiert von Kulturscheune Liestal | Oltingen 
<span class="dk">DILL & KRAUT - Kaserne Lokal</span>  | <span class="dk">Kaserne Basel </span>  

</div>
      </div>
    </div>
  </div>




<div class="accordion-item accordion-year my-3">
    <h2 class="accordion-header" id="flush-headingOne">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse19" aria-expanded="false" aria-controls="flush-collapse19">
        2019
      </button>
    </h2>
    <div id="flush-collapse19" class="accordion-collapse collapse" aria-labelledby="flush-heading19" data-bs-parent="#accordionFlushExample">
      <div class="accordion-body">
<div class="table-responsive">

|  |  |  
---- | ---- | ----- | 
31.10. | Slam Poetry zur Sonderausstellung GLADIATOREN | Antikenmuseum, Basel 
08.09. | SPOKEN WORD | Frauenrechte beider Basel, Basel 
29.06. | SPOKEN WORD | Kellertheater Brig 
05.06. | SPOKEN WORD | Kunstmuseum, Luzern 
14.04. | Schopfheimer Mundart-Literatur-Werkstatt |  Allgemeine Lesegesellschaft, Basel 
13.04. | Schopfheimer Mundart-Literatur-Werkstatt | St. Agathe, Schopfheim 
12.04. | Schopfheimer Mundart-Literatur-Werkstatt | Stapflehus, Weil a. Rhein 
08.02. | Eröffnung WORTSTELLWERK | schmatz, Basel 
04.02. | Präsentation Schulhausroman | Theater Teufelhof, Basel 
28.01. | 54. Solothurner Filmtage | Canva, Solothurn 
26.01. | 54. Solothurner Filmtage | Reithalle, Solothurn 
23.01 | 54. Solothurner Filmtage, projection spéciale | Reithalle, Solothurn 
      </div>
      </div>
    </div>
  </div>
  </div>
  </div></div>



<div class="accordion-item accordion-year my-3">
    <h2 class="accordion-header" id="flush-headingOne">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse18" aria-expanded="false" aria-controls="flush-collapse19">
        2018
      </button>
    </h2>
    <div id="flush-collapse18" class="accordion-collapse collapse" aria-labelledby="flush-heading18" data-bs-parent="#accordionFlushExample">
      <div class="accordion-body">
<div class="table-responsive">

|  |  |  
---- | ---- | ----- | 
14.12. | Dichter SLAM | Dichter- und Stadtmuseum, Liestal 
11.12. | Spoken Word | KV Liestal 
07.12. | ZEBRA - Poetry Film Festival | Kulturbrauerei, Berlin (D) 
29.11. | Spoken Word | KV Business School, Zürich 
27.11. | Spoken Word | KV Business School, Zürich 
15.11. | Abend mit Les amis de l'Alsace Bâle | Schmiedenhof, Basel 
07.11. | Spoken Word | BUCH WIEN, Wien 
26.10. | Preacher Slam (MC) | Matthäus Kirche, Basel 
21.10. | Lesung mit Donat Blum (MC) | Sofa-Lesung, Basel 
20.10. | Poetry Slam | Theater im Vogelsang, Altdorf 
19.10. | Spoken Word (MC) | woerdz Festival, Luzern 
29.09. | ZEBRA - Poetry Film Festival | Münster (D) 
21.09. | 10 Jahre Jubiläum der beobachtungsstelle.ch | PROGR, Bern 
14.09. | Spoken Word am 'Guck mal, Günther, Kunst' | Tommasini, Lenzburg 
05.09. | Poetry Slam (MC) | Gym Liestal, Liestal 
01.09. | U20-Poetry-Slam AG (MC) | Bar im Stall, Aarau 
27.08. | Hommage an Ernst Eggimann: aber hütt (2012) | Radio SRF 1 
28.05. | Queer Slam | Kosmos, Zürich 
18.05. | Bilingue Slam | Tojo Theater, Bern 
17.05. | Jazz Slam | Millers, Zürich 
15.05. | Spoken Word | Dichtermuseum, Liestal 
13.05. | Schnabelweid | Solothurner Literaturtage, Solothurn 
06.05. | Science Fiction Festival | Sommercasino, Basel 
20.04. | Dichter SLAM | Dichter- und Stadtmuseum, Liestal 
17.03. | Spoken Word | Flatterschaft, Basel 
11.03. | Spoken Word | Kirche Enge, Zürich 
09.04. | U20-Slam Finale (MC) | Literaturhaus, Basel 
08.04. | Open-List-Slam | Parterre One, Basel 
06.03. | Spoken Word | Loge, Luzern 
03.03. | Poetry Slam | Affoltern 
02.03. | Laut&Deutlich-Slam | Schützi, Olten 
13.02. | 13 Sterne Slam | Goms 
05.02. | Spoken Word | Radio LoRa, Zürich 
23.01. | Vernissage Stadtbuch CMS | Kaserne, Basel 
16.01. | Spoken Word | Café Littéraire, Aarau 
11.01. | Spoken Word am Berner Präsidialfest | Gaschessu, Bern   
      </div>
      </div>
    </div>
  </div>
  </div>
  </div>

</div>

</div>



